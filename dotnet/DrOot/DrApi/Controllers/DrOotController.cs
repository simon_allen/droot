﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DrLib;
using DrApi.Models;

namespace DrApi.Controllers
{
    public class DrOotController : ApiController
    {
        public DrOot Get(string id)
        {
            DrOot drOot = new DrOot();
            drOot.Input = id;
            drOot.DigitalRoot = DrLibFactory.GetDefault().Reduce(drOot.Input);
            return drOot;
        }
    }
}
