﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrApi.Models
{
    public class DrOot
    {
        public string Input { get; set; }
        public int DigitalRoot { get; set; }
    }
}