﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrLib;
using System;

namespace DrLib.Tests
{
    [TestClass]
    public class TestDrCharMap
    {
        static IDrCharMap map;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            map = DrCharMapFactory.Get();
        }

        [TestMethod]
        public void TestGetMappedChar()
        {
            Assert.AreEqual(1, map.GetMappedChar('a'));
        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\charMapTestData.csv", "charMapTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestGetMappedChar()
        {
            char input = TestContext.DataRow.IsNull("input")?' ': Convert.ToChar(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected")?0: Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, map.GetMappedChar(input), "expected:" + expected + " for input:" + input);
        }
    }
    
    [TestClass]
    public class TestDrCharMapVowels
    {
        static IDrCharMap map;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            map = DrCharMapFactory.Get(DrCharMapEnum.VOWELS);
        }

        [TestMethod]
        public void TestDrCharMapVowel()
        {
            Assert.AreEqual(1, map.GetMappedChar('a'));
            Assert.AreEqual(5, map.GetMappedChar('E'));
            Assert.AreEqual(0, map.GetMappedChar('b'));
        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\charMapVowelsTestData.csv", "charMapVowelsTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestGetMappedChar()
        {
            char input = TestContext.DataRow.IsNull("input") ? ' ' : Convert.ToChar(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected") ? 0 : Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, map.GetMappedChar(input), "expected:" + expected + " for input:" + input);
        }
    }

    [TestClass]
    public class TestDrCharMapConsonants
    {
        static IDrCharMap map;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            map = DrCharMapFactory.Get(DrCharMapEnum.CONSONANTS);
        }

        [TestMethod]
        public void TestDrCharMapVowel()
        {
            Assert.AreEqual(0, map.GetMappedChar('a'));
            Assert.AreEqual(0, map.GetMappedChar('E'));
            Assert.AreEqual(2, map.GetMappedChar('b'));
            Assert.AreEqual(6, map.GetMappedChar('f'));
        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\charMapConsonantsTestData.csv", "charMapConsonantsTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestGetMappedChar()
        {
            char input = TestContext.DataRow.IsNull("input") ? ' ' : Convert.ToChar(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected") ? 0 : Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, map.GetMappedChar(input), "expected:" + expected + " for input:" + input);
        }
    }
}
