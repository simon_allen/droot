﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrLib;
using System;

namespace DrLib.Tests
{
    [TestClass]
    public class TestDrLib
    {
        static IDrLib drLib;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            drLib = DrLibFactory.GetDefault();
        }

        [TestMethod]
        public void TestReduceNums()
        {
            int[] array1 = { 1, 2, 3 };
            Assert.AreEqual(6, drLib.Reduce(array1));
            int[] array2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Assert.AreEqual(9, drLib.Reduce(array2));
        }

        [TestMethod]
        public void TestReduceString()
        {
            Assert.AreEqual(7, drLib.Reduce("Simon"));
        }

        [TestMethod]
        public void TestReduceStringWithNumbers()
        {
            Assert.AreEqual(8, drLib.Reduce("Simon1"));
        }
        
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\reductionTestData.csv", "reductionTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestReduce()
        {
            int input = TestContext.DataRow.IsNull("input") ? 0 : Convert.ToInt32(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected") ? 0 : Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, drLib.Reduce(input), "expected:" + expected + " for input:" + input);
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\reductionStringTestData.csv", "reductionStringTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestReduceString()
        {
            String input = TestContext.DataRow.IsNull("input") ? "" : Convert.ToString(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected") ? 0 : Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, drLib.Reduce(input), "expected:" + expected + " for input:" + input);
        }
    }
}
