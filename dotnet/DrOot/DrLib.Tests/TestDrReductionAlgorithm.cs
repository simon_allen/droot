using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrLib;
using System;

namespace DrLib.Tests
{
    [TestClass]
    public class TestDrReductionAlgorithm
    {
        static IDrReductionAlgorithm _alg;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            _alg = DrReductionAlgorithmFactory.Get();
        }

        [TestMethod]
        public void TestReduce1()
        {
            Assert.AreEqual(1, _alg.Reduce(1));
        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\reductionTestData.csv", "reductionTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestReduce()
        {
            int input = TestContext.DataRow.IsNull("input") ? 0 : Convert.ToInt32(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected") ? 0 : Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, _alg.Reduce(input), "expected:" + expected + " for input:" + input);
        }
    }

    [TestClass]
    public class TestDrReductionAlgorithmMagicNumbers {
        static IDrReductionAlgorithm _alg = null;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            _alg = DrReductionAlgorithmFactory.Get(DrReductionAlgorithmEnum.MAGIC_NUMBERS);
        }

        [TestMethod]
        public void TestReduceWithMagicNumbers()
        {
            Assert.AreEqual(11, _alg.Reduce(11));
            Assert.AreEqual(3, _alg.Reduce(12));
            Assert.AreEqual(22, _alg.Reduce(22));
        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"TestData\reductionMagicNumbersTestData.csv", "reductionMagicNumbersTestData#csv", DataAccessMethod.Sequential)]
        [TestMethod]
        public void DataCsvTestReduce()
        {
            int input = TestContext.DataRow.IsNull("input") ? 0 : Convert.ToInt32(TestContext.DataRow["input"]);
            int expected = TestContext.DataRow.IsNull("expected") ? 0 : Convert.ToInt32(TestContext.DataRow["expected"]);

            Assert.AreEqual(expected, _alg.Reduce(input), "expected:" + expected + " for input:" + input);
        }

    }

}
