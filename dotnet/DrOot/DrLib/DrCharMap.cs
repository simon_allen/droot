﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DrLib
{
    public interface IDrCharMap
    {
        int GetMappedChar(char chr);
    }
    public abstract class DrCharMap : IDrCharMap
    {
        protected abstract Dictionary<char, int> GetCharMap();
        protected bool IsCaseSensitive() { return false; }

        public int GetMappedChar(char chr)
        {
            if(!IsCaseSensitive())
            {
                chr = Char.ToLower(chr);
            }
            if(!GetCharMap().ContainsKey(chr))
            {
                return 0;
            }
            return GetCharMap()[chr];
        }
    }

    public class DrCharMapPythag : DrCharMap
    {
        protected Dictionary<char, int> _charMap = new Dictionary<char, int>()
        {
            ['a'] = 1,
            ['b'] = 2,
            ['c'] = 3,
            ['d'] = 4,
            ['e'] = 5,
            ['f'] = 6,
            ['g'] = 7,
            ['h'] = 8,
            ['i'] = 9,
            ['j'] = 1,
            ['k'] = 2,
            ['l'] = 3,
            ['m'] = 4,
            ['n'] = 5,
            ['o'] = 6,
            ['p'] = 7,
            ['q'] = 8,
            ['r'] = 9,
            ['s'] = 1,
            ['t'] = 2,
            ['u'] = 3,
            ['v'] = 4,
            ['w'] = 5,
            ['x'] = 6,
            ['y'] = 7,
            ['z'] = 8
        };
        protected override Dictionary<char, int> GetCharMap()
        {
            return _charMap;
        }
    }

    public class DrCharMapVowels : DrCharMap
    {
        protected Dictionary<char, int> _charMap = new Dictionary<char, int>()
        {
            ['a'] = 1,
            ['e'] = 5,
            ['i'] = 9,
            ['o'] = 6,
            ['u'] = 3,
            ['y'] = 7
        };
        protected override Dictionary<char, int> GetCharMap()
        {
            return _charMap;
        }
    }

    public class DrCharMapConsonants : DrCharMap
    {
        protected Dictionary<char, int> _charMap = new Dictionary<char, int>()
        {
            ['b'] = 2,
            ['c'] = 3,
            ['d'] = 4,
            ['f'] = 6,
            ['g'] = 7,
            ['h'] = 8,
            ['j'] = 1,
            ['k'] = 2,
            ['l'] = 3,
            ['m'] = 4,
            ['n'] = 5,
            ['p'] = 7,
            ['q'] = 8,
            ['r'] = 9,
            ['s'] = 1,
            ['t'] = 2,
            ['v'] = 4,
            ['w'] = 5,
            ['x'] = 6,
            ['z'] = 8
        };
        protected override Dictionary<char, int> GetCharMap()
        {
            return _charMap;
        }
    }

    public enum DrCharMapEnum { DEFAULT, VOWELS, CONSONANTS };
    public static class DrCharMapFactory
    {
        public static IDrCharMap Get()
        {
            return GetInstance();
        }
        static IDrCharMap GetInstance() { return new DrCharMapPythag(); }
        static IDrCharMap GetInstanceVowels() { return new DrCharMapVowels(); }
        static IDrCharMap GetInstanceConsonants() { return new DrCharMapConsonants(); }
        public static IDrCharMap Get(DrCharMapEnum charMap)
        {
            switch(charMap)
            {
                case DrCharMapEnum.VOWELS:
                    return GetInstanceVowels();
                case DrCharMapEnum.CONSONANTS:
                    return GetInstanceConsonants();
                default:
                    return GetInstance();
            }
        }
    }
}
