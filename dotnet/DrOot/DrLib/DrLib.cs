﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DrLib
{
    public interface IDrLib
    {
        int Reduce(int num);
        int Reduce(int[] nums);
        int Reduce(string str);
    }

    public class DrLib : IDrLib
    {
        protected IDrReductionAlgorithm _alg;
        protected IDrCharMap _charMap;
        public DrLib(IDrReductionAlgorithm alg, IDrCharMap charMap)
        {
            _alg = alg;
            _charMap = charMap;
        }

        public int Reduce(int num)
        {
            return _alg.Reduce(num);
        }

        public int Reduce(int[] nums)
        {
            int total = 0;
            foreach (int num  in nums)
            {
                total += num;
            }
            return _alg.Reduce(total);
        }

        public int Reduce(string str)
        {
            int total = 0;
            foreach (char chr in str)
            {
                int num = Char.IsDigit(chr) ? Convert.ToInt32(Char.GetNumericValue(chr)) : _charMap.GetMappedChar(chr);
                total += num;
            }
            return _alg.Reduce(total);
        }
    }

    public static class DrLibFactory
    {
        public static IDrLib GetDefault()
        {
            return new DrLib(DrReductionAlgorithmFactory.Get(), DrCharMapFactory.Get());
        }
    }
}
