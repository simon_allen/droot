﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DrLib
{
    public interface IDrReductionAlgorithm
    {
        int Reduce(int num);
    }

    public class DrReductionAlgorithm : IDrReductionAlgorithm
    {
        private int[] _magicNumbers = new int[0];

        public DrReductionAlgorithm() { }
        public DrReductionAlgorithm(int[] magicNumbers)
        {
            if(magicNumbers != null) _magicNumbers = magicNumbers;
        }

        protected bool IsMagicNumber(int num)
        {
            if (_magicNumbers == null)
            {
                return false;
            }
            return Array.Exists(_magicNumbers, element => element == num);
        }

        public int Reduce(int num)
        {
            while (num > 9 && !IsMagicNumber(num))
            {
                num = GetDigitSum(num);
            }
            return num;
        }

        protected int GetDigitSum(int num)
        {
            int total = 0;
            while (num > 0)
            {
                int digit = num % 10;
                num /= 10;
                total += digit;
            }
            return total;
        }
    }

    public class DrReductionAlgorithmMod9 : IDrReductionAlgorithm
    {
        public int Reduce(int num)
        {
            throw new NotImplementedException();
        }
    }

    public enum DrReductionAlgorithmEnum { DEFAULT, MAGIC_NUMBERS, MOD9 };
    public static class DrReductionAlgorithmFactory
    {
        static int[] _magicNumbers = { 11, 22 };
        public static IDrReductionAlgorithm Get()
        {
            return GetInstance();
        }

        public static IDrReductionAlgorithm GetMagicNumbers()
        {
            int[] magicNumbers = { 11, 22 };
            return new DrReductionAlgorithm( magicNumbers );
        }
        static IDrReductionAlgorithm GetInstance() { return new DrReductionAlgorithm(); }
        static IDrReductionAlgorithm GetInstanceMagicNumbers() { return new DrReductionAlgorithm(_magicNumbers); }
        static IDrReductionAlgorithm GetInstanceMod9() { return new DrReductionAlgorithmMod9(); }
        public static IDrReductionAlgorithm Get(DrReductionAlgorithmEnum alg)
        {
            switch (alg)
            {
                case DrReductionAlgorithmEnum.MAGIC_NUMBERS:
                    return GetInstanceMagicNumbers();
                case DrReductionAlgorithmEnum.MOD9:
                    return GetInstanceMod9();
                default:
                    return GetInstance();
            }
        }
    }
}
